import React from "react";

import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";

// import { display } from "@material-ui/system";

import {
  Box,
  TextField,
  Tooltip,
  Typography,
  List,
  ListItem,
  ListItemText
} from "@material-ui/core";

import { OipApi } from "oip/OipApi";
import { config } from "ddx.config.js";

// "tmpl_636E68FA"

const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    flexDirection: "row"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  avatar: {
    maxWidth: "125px",
    border: "solid"
  },
  thumb: {
    maxWidth: "75px",
    border: "solid"
  },
  address: {
    fontSize: "0.1rem"
  },
  cardFooter: {
    paddingTop: "0rem",
    border: "0",
    borderRadius: "6px",
    justifyContent: "center !important"
  }
});

class Organization extends React.Component {
  state = {
    update: true,
    location: "",
    parentOrganizationList: [],
    parentOrganizationListName: []
  };

  updateParent = () => {
    this.setState({ update: !this.state.update }, () => {
      const payload = {
        update: this.state.update,
        location: this.state.location,
        parentOrganizationList: this.state.parentOrganizationList
      };
      this.props.onChangePayload(payload);
    });
  };

  handleChange = name => event => {
    console.log(`updating ${name} to ${event.target.value}`)
    this.setState({ [name]: event.target.value }, this.updateParent());
  };

  handleChangeOip = (name, checkOip) => event => {
    const input = event.target.value;
    console.log(input)
    this.setState({ [name]: input }, () =>
      checkOip ? this.setName(name, checkOip) : this.updateParent()
    );
  };

  handleChangeOipList = (name, checkOip) => event => {
    const input = event.target.value.split("\n");
    while (input.indexOf("") !== -1) {
      const rm = input.indexOf("");
      input.splice(rm, 1);
    }
    this.setState({ [name]: input }, () =>
      checkOip ? this.setNames(name, checkOip) : this.updateParent()
    );
  };

  setName = async name => {
    console.log("setName");
    const id = this.state[name];
    console.log(id);
    const n = await this.oip2name(id);
    this.setState({ [`${name}Name`]: n }, () => this.updateParent());
  };


  setNames = async name => {
    console.log("setNames");
    const names = [];
    const ids = this.state[name];
    console.log(ids);
    for (let id of ids) {
      const name = await this.oip2name(id);
      if (name !== "") {
        names.push(name);
      }
    }
    this.setState({ [`${name}Name`]: names }, () => this.updateParent());
  };

  oip2name = id => {
    const oip = new OipApi();
    console.log(id);
    return oip.getRecord(id).then(record => {
      console.log(record);
      if (record.results) {
        if (record.results.length === 1 && record.results[0].meta.txid === id) {
          const name =
            record.results[0].record.details[config.cardInfo.name.tmpl][
              config.cardInfo.name.name
            ];
          console.log(`record: ${JSON.stringify(name)}`);
          return name;
        }
        console.log(`no record found!`);
        return "";
      }
      console.log("not valid id");
      return "";
    });
  };

  renderValue = (component, label) => {
    const { classes } = this.props;
    return (
      <TextField
        id={`standard-by--${component}`}
        label={label}
        defaultValue=""
        fullWidth
        multiline
        onChange={this.handleChange(component)}
        InputLabelProps={{
          className: classes.input,
          shrink: true
        }}
      />
    );
  };

  renderValueOip = (component, label, checkOip = true) => {
    const { classes } = this.props;
    return (
      <TextField
        id={`standard-by-${label}`}
        label={label}
        defaultValue=""
        fullWidth
        multiline
        onChange={this.handleChangeOip(component, checkOip)}
        InputLabelProps={{
          className: classes.input,
          shrink: true
        }}
      />
    );
  };

  renderValueOipDisplay = (component, label, display) => {
    return (
      <Box display="flex" width={1} flexGrow={1} flexDirection="row">
        <Box p={1} width={1 / 4} flexGrow={1} flexDirection="column">
          <Box>
            <Typography variant="caption">{display}</Typography>
          </Box>
        </Box>
        <Box p={1} width={3 / 4} flexGrow={1}>
          {this.renderValueOip(component, label)}
        </Box>
      </Box>
    );
  };

  renderValueList = (component, label, checkOip = true) => {
    const { classes } = this.props;
    return (
      <TextField
        id={`standard-by-${label}`}
        label={label}
        defaultValue=""
        fullWidth
        multiline
        onChange={this.handleChangeOipList(component, checkOip)}
        InputLabelProps={{
          className: classes.input,
          shrink: true
        }}
      />
    );
  };

  renderValueListDisplay = (component, label, display) => {
    return (
      <Box display="flex" width={1} flexGrow={1} flexDirection="row">
        <Box p={1} width={1 / 4} flexGrow={1} flexDirection="column">
          {display.map((d, i) => (
            <Box key={i}>
              <Typography variant="caption">{d}</Typography>
            </Box>
          ))}
        </Box>
        <Box p={1} width={3 / 4} flexGrow={1}>
          {this.renderValueList(component, label)}
        </Box>
      </Box>
    );
  };

  renderDefault = () => {
    return (
      <Box
        justifyContent="center"
        flexDirection={"column"}
        display="flex"
        flexGrow={1}
        width={1}
      >
        <Box p={1} width={{ xs: 1 }}>
          {this.renderValue("location", "Location")}
        </Box>
        <Box p={1} width={{ xs: 1 }}>
          {this.renderValueListDisplay(
            "parentOrganizationList",
            "Parent Organizations",
            this.state.parentOrganizationListName
          )}
        </Box>
        {/*         <Box display="flex" flexDirection={"column"} width={1}>
          {this.renderValueListDisplay(
            "parentOrganizationList",
            "Parent Organizations",
            this.state.parentOrganizationList
          )}
        </Box> */}
      </Box>
    );
  };

  render() {
    const { component } = this.props;
    switch (component) {
      case "location":
        return this.renderValue(component);
      case "parentOrganizationList":
        return this.renderValueListDisplay(component);
      default:
        return this.renderDefault();
    }
  }
}

Organization.propTypes = {
  classes: PropTypes.object.isRequired,
  onChangePayload: PropTypes.func,
  component: PropTypes.string,
  label: PropTypes.string
};

export default withStyles(styles)(Organization);
